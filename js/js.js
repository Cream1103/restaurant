﻿function timestamp(){
    return new Date().getTime();
}

$(document).ready(function(){
    $('.relative_class_for_hide_element').mouseover(function(){
       $(this).find('.hide_title').stop().slideDown(200);
    });
    $('.relative_class_for_hide_element').mouseout(function(){
        $(this).find('.hide_title').stop().slideUp(200);
    });




	$('select.selectmenu').selectmenu(); // dont't comment



	$('.alert .close').click(function() {
		$('#message-container').fadeOut(500);
	});


	$(document).on('submit', '#post-comment-form', function (event) {
		$.ajax({
			url: $(this).attr('action') + '?timestamp=' + timestamp(),
			data: $(this).serialize(),
			type: "POST",
			success: function (data) {
				$('#card_omok_reviews').html(data);
				return false;

			},
		});
		return false;
	});



// RadioButton



	$(document).on('click', '.radioblock_1 .radio_1', function(event) {
    //$('.radioblock_1').find('.radio_1').each(function(){
    //    $(this).click(function(){
            var valueRadio_1 = $(this).attr('rel');
            $(this).parent().find('.radio_1').removeClass('active');
            $(this).addClass('active');
            $(this).parent().find('.hidd_inputs').val(valueRadio_1);
        //});
    });



    $('.one_filters_block ul li a').click(function(){
//        $(this).parents('.one_filters_block').find('li').removeClass('active');
        $(this).parents('li').addClass('active')
    });
    $('.span_closes').click(function(){
       $(this).parents('li').removeClass('active');
    });


       //ПОПАПЫ
    $('.open_popup').click(function() {
        var popup_id = $('#' + $(this).attr("rel"));
        $(popup_id).show();
        $('.overlay').show();
    });
    $('.popup .close, .overlay').click(function() {
        $('.overlay, .popup').hide();
    });
    //ПОПАПЫ

// RadioButton
	$(document).on('click', '.radio', function(event) {
    //$('.radio').click(function(){
            var valueRadio = $(this).html();
            $(this).parents('.radioblock').find('.radio').removeClass('active');
            $(this).addClass('active');
            $(this).parents('.radioblock').find('input').val(valueRadio);
    });


    // Select
	$(document).on('click', '.slct', function(event) {
    //$('.slct').click(function(){
        $('.drop').slideUp();
        $('.slct').removeClass('active');
        var dropBlock = $(this).parent().find('.drop');
        if( dropBlock.is(':hidden') ) {
            dropBlock.slideDown();
            $(this).addClass('active');
            $('.drop').find('li').click(function(){
                var selectResult = $(this).html();
                var selectResult2 = $(this).attr('rel');
                $(this).parent().parent().find('input').val(selectResult2);
                $(this).parent().parent().find('.slct').removeClass('active').html(selectResult);
                dropBlock.slideUp();
            });
        } else {
            $(this).removeClass('active');
            dropBlock.slideUp();
        }
        return false;
    });


    // Checkbox
	$(document).on('click', '.checkboxes .check', function(event) {
    //$('.checkboxes').find('.check').click(function(){
        if( $(this).find('input').hasClass('active_input') ) {
            $(this).removeClass('active');
            $(this).find('input').removeAttr('checked').removeClass('active_input');
        } else {
            $(this).addClass('active');
            $(this).find('input').attr('checked', 'checked').addClass('active_input');
        }
    });


    $('.checkboxes_all').find('.check_all').click(function(){
        if( $(this).find('input').hasClass('active_input') ) {

            $(this).removeClass('active');
            $(this).find('input').removeAttr('checked').removeClass('active_input');

            $('.table_dis_v').find('.check').removeClass('active');
            $('.table_dis_v').find('input').removeAttr('checked').removeClass('active_input');
        } else {
            $(this).addClass('active');
            $(this).find('input').attr('checked', 'checked').addClass('active_input');

            $('.table_dis_v').find('.check').addClass('active');
            $('.table_dis_v').find('input').attr('checked', 'checked').addClass('active_input');
        }
    });


    // = Load
// отслеживаем изменение инпута file
    $('#file').change(function(){
        // Если файл прикрепили то заносим значение value в переменную
        var fileResult = $(this).val();
        // И дальше передаем значение в инпут который под загрузчиком
        $(this).parent().find('.fileLoad').find('input').val(fileResult);
    });

    /* Добавляем новый класс кнопке если инпут файл получил фокус */
    $('#file').hover(function(){
        $(this).parent().find('button').addClass('button-hover');
    }, function(){
        $(this).parent().find('button').removeClass('button-hover');
    });

















//	//$("select.head_sel, select.kol_people, select.kol_zav").sb();
//	$("select.head_sel, select.kol_people, select.kol_zav").selectmenu();

	//$('#slider_rest').nivoSlider({
	//	effect: 'fade',
	//	controlNav: false,
	//	directionNavHide: false,
	//	pauseOnHover: true,
	//	captionOpacity: 1,
	//	prevText: '<',
	//	nextText: '>'
	//});

	$(".inp.people span").click(function(){
		if (($(this).hasClass("top"))&&($(this).parent().find("input").val() < 10)){
			$(this).parent().find("input").val(parseInt($(this).parent().find("input").val()) + 1)
		}
		if ($(this).hasClass("bottom")){
			if($(this).parent().find("input").val() > 1){
				$(this).parent().find("input").val(parseInt($(this).parent().find("input").val()) - 1)
			}
		}

		if ($(".input_people").val()>4){
			$("span.text_p").text("персон");
		}  else {
			$("span.text_p").text("персоны");
			if ($(".input_people").val()==1){
				$("span.text_p").text("персона");
			}  else {
				$("span.text_p").text("персоны");
			}
		}

	});

	var month = 02;
	var day = 20;
	var year = 13;
	var i = 0;

	$(".inp.date .top").click(function(){
		i++;
		$(".input_date").datepicker({dateFormat:'dd.mm.yy'});
		var a = $( ".input_date" ).datepicker( "getDate" );
		a.setDate(a.getDate()+1);
		$( ".input_date" ).datepicker( "setDate", a);
	});

	$(".inp.date .bottom").click(function(){
		$(".input_date").datepicker({dateFormat:'dd.mm.yy'});
		var a = $( ".input_date" ).datepicker( "getDate" );
		a.setDate(a.getDate()-1);
		$( ".input_date" ).datepicker( "setDate", a);
	})

	$("span.bg").click(function(){
		if ($(this).hasClass("click_checkbox")){
			$(this).removeClass("click_checkbox");
		} else {
			$(this).addClass("click_checkbox");
		}

	});

	/* Artem */
	function getActive(){
		$(".single_rest .tabs_content .item").css("display","none");
		$(".single_rest .tabs a").each(function(){
			if ($(this).hasClass("active")) {
				$(".single_rest .tabs_content "+$(this).attr("href")).css("display","block");
				if ($(this).attr("href") == '#map')
					initialize_map();
				return false;
			}
		})
	}

	if ($("div").hasClass("tabs_container")) {
		getActive();
	}

	$(".single_rest .tabs li").click(function(){
		$(".single_rest .tabs a").removeClass("active");
		$(this).find("a").addClass("active");
		getActive();
		return false;
	})

	$(".t-title").click(function(){
		return false;
	})
	$(".dd .t-title").click(function(){
		if ($(this).parent().hasClass("yes")){
			$(this).parent().find(".content").slideToggle();
			$(".content.time").slideToggle(300);
		}
		return false;
	});

	$("ul.time-sales li").on('click',function(){
		$("ul.time-sales li").removeClass("active");
		$(this).addClass("active");
		$("input.inf_time").val($(this).attr("time-id"));
	});
	$(".selectbox.kol_people .item").on("click",function(){
		$(".content.dd").slideDown(300);
		$(".content.time").slideUp(300);
		$($(".horisontal-tab.dd").addClass("yes"))
	});
	$(".content.dd").datepicker({                                 
		minDate: new Date(),
		dateFormat: 'dd.mm.yy',
		onSelect: function(dateText, inst) {
			date = $(".hasDatepicker ").val();
			$("input.inf_date, input.in_dd").val(date);
			set_times(date);
			$(".content.dd").slideUp(300);
			$(".content.time").slideDown(300);
			$(".content.dd").addClass("open");
		}
	});
	if ($("div").hasClass("single_rest_container")){
		if ($("input[name='d']").val()!="") {
			$(".horisontal-tab.dd").addClass("yes");
			$(".in_dd").val($("input[name='d']").val());
			$(".content.time").css("display","block");
		}
	}

	$(".booking-btn").click(function(){
		var k=0;
		if ($("input[name='d']").val()=="") k++;
		if ($("input[name='t']").val()=="") k++;
		if ($("select.kol_people option:selected").val()=="") k++;
		if (k!=0) return false;
	});

	$(".view-all").click(function(){
		if($(".content.description").hasClass("show")){
			$(".content.description").removeClass("show");
			$(".view-all").text("Показать описание полностью");
		}
		else{
			$(".content.description").addClass("show");
			$(".view-all").text("Свернуть");
		}
		return false;
	});
	$(".t-title.cont").click(function(){
		$(this).parent().find(".content").slideToggle();
		$(this).find(".pointer").toggleClass("open");
	});


//	вызов функции центрирования при готовности DOM
	alignCenter($(".wr_r"));
	// вызов функции при ресайзе окна
	$(window).resize(function() {
		alignCenter($(".wr_r"));
	})

	// функция центрирования элемента

	var i = 0;
	var adres = window.location.href;
	var mas = adres.split('#');
	var znach = mas[mas.length-1];
	mas[0];
	mas[mas.length-1];
	if(znach == "feedbacks"){
		$(".tabs").find("li:eq(2)").trigger("click");
	}

	if($(".h_text").height() <= 98){
		$(".item").find(".view-all").hide();
	}else{
		$(".item").find(".view-all").show();
	}



});

function vivod(hour,minute){
	if (minute < 10){
		if (hour < 10){
			$(".input_time").val("0" + hour +" : 0"+ minute);
		}else{
			$(".input_time").val(hour +" : 0"+ minute);
		}
	}else{
		if (hour < 10){
			$(".input_time").val("0"+hour +" : "+ minute);
		} else
			$(".input_time").val(hour +" : "+ minute);
	}
}

function alignCenter(elem) {
	elem.css({
		// вычисление координат left и top
		marginTop: ($(window).height() - elem.height()) / 2.6 + 'px'
	})
}

function set_times(date){
	
	cafe = $('input[name="c"]').val();
	
	$.ajax({
		url:'/orders/tfd/' + cafe + '/' + date + '/',
		success: function(data){
			$('.content.time').html(data);
		}
	})
} 
